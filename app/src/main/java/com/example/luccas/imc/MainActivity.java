package com.example.luccas.imc;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void Calcular(View v) {
        EditText inputMassa = (EditText) findViewById(R.id.input_massa);
        EditText inputAltura = (EditText) findViewById(R.id.input_altura);
        TextView result = (TextView) findViewById(R.id.resultado_imc);
        String classificacao;

        double massa = Double.parseDouble(inputMassa.getText().toString());

        double altura = Double.parseDouble(inputAltura.getText().toString());

        double imc = massa / (altura * altura);

        if (imc < 18.5) {
            //Abaixo do peso
            classificacao = "Abaixo do Peso";
        } else if (imc > 18.5 && imc < 24.9) {
            //Peso Ideal
            classificacao = "Peso Ideal (Parabens)";
        } else if (imc > 24.9 && imc < 29.9) {
            //Acima do Peso
            classificacao = "Levemente Acima do Peso";
        } else if (imc > 29.9 && imc < 34.9) {
            //Obesidade Grau 1
            classificacao = "Obesidade Grau 1";
        } else if (imc > 34.9 && imc < 39.9) {
            //Obesidade Grau 2
            classificacao = "Obesidade Grau 2 (Severa)";
        } else {
            //Obesidade Grau 3
            classificacao = "Obesidade Grau 3 (Morbida)";
        }
        result.setText("IMC = " + String.format("%.2f", imc) + " " + classificacao);
    }
}
